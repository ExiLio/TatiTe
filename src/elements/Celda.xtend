package elements

import org.eclipse.xtend.lib.annotations.Accessors
import excepciones.MovidaInvalidaException

@Accessors
class Celda {
	
	Pieza pieza
	
	new () {
		pieza = Pieza.VACIO
	}
	
	def colocarPieza(Pieza pieza) {
		if (this.pieza.equals(Pieza.VACIO))
			setPieza(pieza)
		else
			throw new MovidaInvalidaException("No puede colocar una pieza en esta posicion")
	}
	
	def reemplazarPieza (Pieza pieza){
		setPieza(pieza)
	}
	
	def piezaEnPosicion() {
		getPieza
	}
	
	def quitarPieza(){
		setPieza(Pieza.VACIO)
	}
	
	def estaVacio() {
		return this.pieza.equals(Pieza.VACIO)
	}
	
}