package elements

import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class Fila {
	
	List<Celda> celdas;
	
	new () {
	}
	
	new (Integer nroCeldas) {
		celdas = newArrayList
		for (var j=0 ; j<nroCeldas ; j++) {
			var celda = new Celda
			celdas.add(j,celda)
		}
	}
	
	def colocarPieza (Pieza pieza , Integer posFila) {
		celdas.get(posFila).colocarPieza(pieza)
	}
	
	def piezaEnPosicion (Integer posFila) {
		celdas.get(posFila).piezaEnPosicion()
	}
	
	def reemplazarPieza (Pieza pieza , Integer posFila) {
		celdas.get(posFila).reemplazarPieza(pieza)
	}
	
	def quitarPieza (Integer posFila) {
		celdas.get(posFila).quitarPieza()
	}
	
	def Integer piezasColocadas() {
		var cant=0;
		for (Celda c : celdas){
			if (! c.estaVacio) {
				cant = cant +1
			}
		}
		cant
	}
	
}