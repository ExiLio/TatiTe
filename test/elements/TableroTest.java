package elements;

import static org.junit.Assert.*;

import excepciones.*;
import org.junit.Before;
import org.junit.Test;

public class TableroTest {

	Tablero sut;
	
	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void testCreacionDeTableroCorrecta() {
		sut = new Tablero(5);
		
		Integer expected = 25;
		Integer current = sut.cantidadDeCeldas();
		
		assertEquals(expected,current);
	}
	
	@Test
	public void testPiezasSeColocanEnPosicionCorrecta() {
		sut = new Tablero(2);
		Posicion posicion = new Posicion(1,2);
		sut.colocarPieza(Pieza.CRUZ , posicion);
		
		Pieza expected = Pieza.CRUZ;
		Pieza current = sut.piezaEnPosicion(posicion);
		
		assertEquals(expected,current);
	}
	
	@Test
	public void testPiezasSeEliminanDeFormaCorrecta() {
		sut = new Tablero(3);
		Posicion posicion = new Posicion(3,3);
		sut.colocarPieza(Pieza.CIRCULO, posicion);
		sut.quitarPieza(posicion);
		
		Pieza expected = Pieza.VACIO;
		Pieza current = sut.piezaEnPosicion(posicion);
		
		assertEquals(expected,current);
	}
	
	@Test
	public void testPiezasSeReemplazanCorrectamente() {
		sut = new Tablero(3);
		Posicion posicion = new Posicion(3,3);
		sut.colocarPieza(Pieza.CIRCULO, posicion);
		sut.reemplazarPieza(Pieza.CRUZ,posicion);
		
		Pieza expected = Pieza.CRUZ;
		Pieza current = sut.piezaEnPosicion(posicion);
		
		assertEquals(expected,current);
	}
	
	@Test (expected = MovidaInvalidaException.class)
	public void testSiSeColocaPiezaDondeYaHayLanzaExcepcion() {
		sut = new Tablero(3);
		Posicion posicion = new Posicion(3,3);
		sut.colocarPieza(Pieza.CIRCULO, posicion);
		sut.colocarPieza(Pieza.CRUZ,posicion);
	}
	
	@Test
	public void testTableroIndicaSiEstaVacio() {
		sut = new Tablero(2);
		
		Integer expected = 0;
		Integer current = sut.piezasColocadas();
		
		assertEquals(expected,current);
	}
	

}
