package domain;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import elements.Pieza;
import elements.Posicion;
import excepciones.MovidaInvalidaException;

public class TatetiTest {
	
	Tateti sut;

	@Before
	public void setUp() throws Exception {
		sut = new Tateti();
	}

	@After
	public void tearDown() throws Exception {
		sut.limpiar();
	}

	@Test
	public void testTatetiInicialVacio() {
		assertTrue(sut.vacio());	
	}
	
	@Test
	public void testColocarCruzCorrectamente() {
		Posicion posicion = new Posicion(2,2);
		sut.ponerCruz(posicion);
		
		Pieza expected = Pieza.CRUZ;
		Pieza current = sut.piezaEn(posicion);
		
		assertEquals(current,expected);
	}
	
	@Test
	public void testAlColocarAlguansPiezasElTableroNoEsVacio() {
		sut.ponerCruz(new Posicion(1,1));
		sut.ponerCirculo(new Posicion(2,2));
		sut.ponerCruz(new Posicion(3,3));
		
		Boolean current = sut.vacio();
		
		assertFalse(current);
	}
	
	@Test (expected = MovidaInvalidaException.class)
	public void testNoExisteReemplazoDePiezasEnElTateti() {
		sut.ponerCirculo(new Posicion(1,1));
		sut.ponerCruz(new Posicion(1,1));
		
		fail();
	}
	
	@Test
	public void testTableroPuedeReiniciarse() {
		sut.ponerCirculo(new Posicion(1,1));
		sut.ponerCirculo(new Posicion(1,2));
		sut.ponerCirculo(new Posicion(1,3));
		sut.ponerCirculo(new Posicion(2,1));
		sut.ponerCirculo(new Posicion(2,2));
		sut.ponerCruz(new Posicion(2,3));
		sut.ponerCruz(new Posicion(3,1));
		sut.ponerCruz(new Posicion(3,2));
		sut.ponerCruz(new Posicion(3,3));
		sut.limpiar();
		
		Boolean current = sut.vacio();
		
		assertTrue(current);
	}
	
	@Test
	public void testTatetiConPiezasDeFormaCorrecta() {
		sut.ponerCirculo(new Posicion(1,1));
		sut.ponerCirculo(new Posicion(1,2));
		sut.ponerCirculo(new Posicion(1,3));
		sut.ponerCirculo(new Posicion(2,1));
		sut.ponerCirculo(new Posicion(2,2));
		sut.ponerCruz(new Posicion(2,3));
		sut.ponerCruz(new Posicion(3,1));
		sut.ponerCruz(new Posicion(3,2));
		sut.ponerCruz(new Posicion(3,3));
		
		Boolean current = sut.vacio();
		
		assertFalse(current);
	}
	
	@Test
	public void testDeVisualizacion () {
		sut.ponerCirculo(new Posicion(1,1));
		sut.ponerCirculo(new Posicion(1,2));
		sut.ponerCruz(new Posicion(3,3));
		sut.visualizar();
		
		assertTrue(true);
	}

}
