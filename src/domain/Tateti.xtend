package domain

import elements.Pieza
import elements.Posicion
import elements.Tablero

class Tateti {
	
	Tablero tateti
	
	new (){
		tateti = new Tablero(3)
	}
	
	def vacio() {
		tateti.piezasColocadas == 0 
	}
	
	def ponerCruz(Posicion posicion) {
		tateti.colocarPieza(Pieza.CRUZ , posicion)
	}
	
	def ponerCirculo(Posicion posicion) {
		tateti.colocarPieza(Pieza.CIRCULO , posicion)
	}
	
	def piezaEn(Posicion posicion) {
		tateti.piezaEnPosicion(posicion)
	}
	
	def limpiar() {
		for (var i=1 ; i<=3 ; i++) {
			for (var j=1 ; j<=3 ; j++) {	
				tateti.quitarPieza(new Posicion(i,j))
			}
		}
	}
	
	def visualizar() {
		for (var i=0 ; i<3 ; i++) {
			System.out.println("---------------------")
			for (var j=0 ; j<3 ; j++) {
				System.out.print("-")
				mostrarPieza(tateti.piezaEnPosicion(new Posicion(i+1,j+1)))
			}
			System.out.println(" ")
			System.out.println("---------------------")
		}
	}
	
	
	def mostrarPieza (Pieza pieza) {
		System.out.print(pieza)
	}
}