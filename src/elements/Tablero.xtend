package elements

import org.eclipse.xtend.lib.annotations.Accessors
import java.util.List

@Accessors
class Tablero {
	
	
	List<Fila> columnas;
	
	
	new () {
		
	}
	
	new (Integer nroFilas){
		columnas = newArrayList
		for (var i=0 ; i<nroFilas ; i++) {
			var fila = new Fila(nroFilas)
			columnas.add(i,fila)
		}
	}
	
	def cantidadDeCeldas() {
		columnas.size * columnas.size
	}
	
	def colocarPieza (Pieza pieza , Posicion posicion) {
		colocarPiezaEnFila(pieza , posicion.x-1 , posicion.y-1)
	}
	
	private def colocarPiezaEnFila (Pieza pieza , Integer posColumna , Integer posFila) {
		columnas.get(posColumna).colocarPieza(pieza , posFila)
	}
	
	def piezaEnPosicion (Posicion posicion) {
		piezaEnPosicion(posicion.x-1 , posicion.y-1)
	}
	
	private def piezaEnPosicion (Integer posColumna , Integer posFila) {
		columnas.get(posColumna).piezaEnPosicion(posFila)
	}
	
	def quitarPieza(Posicion posicion){
		quitarPieza(posicion.x-1 , posicion.y-1)
	}
	
	private def quitarPieza (Integer posColumna , Integer posFila) {
		columnas.get(posColumna).quitarPieza(posFila)
	}
	
	def reemplazarPieza(Pieza pieza , Posicion posicion){
		reemplazarPieza(pieza , posicion.x-1 , posicion.y-1)
	}
	
	private def reemplazarPieza (Pieza pieza , Integer posColumna , Integer posFila) {
		columnas.get(posColumna).reemplazarPieza(pieza , posFila)
	}
	
	def piezasColocadas() {
		var cant=0;
		for (Fila f : columnas){
			cant = cant + f.piezasColocadas
		}
		cant
	}
	
}