package elements;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CeldaTest {

	Celda sut;
	
	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void testCreacionDeCeldaValida() {
		sut = new Celda();
		
		Pieza expected = Pieza.VACIO;
		Pieza current = sut.getPieza();
		
		assertEquals(expected, current);
		
	}

}
