package elements

import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class Posicion {
	
	Integer x
	Integer y
	
	new (Integer x , Integer y) {
		this.x = x
		this.y = y
	}
	
}